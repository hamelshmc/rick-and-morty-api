import { render } from '@testing-library/vue';
import BaseButton from '@/components/BaseButton.vue';

describe('BaseButton.vue', () => {
  it('should render button', () => {
    render(BaseButton);
  });
  it('should render button', () => {
    const { getByRole } = render(BaseButton, {
      props: {
        theme: 'primary',
      },
    });

    const button = getByRole('button');
    expect(button.classList).toContain('primary');
  });

  // it('should emit click when button is clicked', async () => {
  //   const { getByRole, emitted } = render(BaseButton);
  //   const button = getByRole('button');
  //   await fireEvent.click(button);
  //   expect(emitted().click).toBeTruthy();
  // });
});
