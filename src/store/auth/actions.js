export default {
  login({ commit }, loginData) {
    const payload = {
      email: loginData.email,
      isLogged: true,
    };
    commit('setUserData', payload);
  },
};
