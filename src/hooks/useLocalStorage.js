import { ref, watch } from '@vue/runtime-core';
export function useLocalStorage(key) {
  let init = localStorage.getItem(key);
  const variable = ref(init ? JSON.parse(init) : []);
  watch(variable, (to) => {
    localStorage.setItem(key, JSON.stringify(to));
  });
  return variable;
}
